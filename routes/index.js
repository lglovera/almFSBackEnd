var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    
    const response = {
        hotels : [{
            name : 'Hotel Emperador',
            picture: 'images/emperador.jpg',
            stars : 3,
            price : 1596
        },{
            name : "Petit Palace San Bernardo",
            picture: 'images/palace.jpg',
            stars : 4,
            price : 2145
        },{
            name : "Hotel Nuevo Boston",
            picture: 'images/boston.jpg',
            stars : 2,
            price : 861
        }]
    };
    
    res.send( JSON.stringify( response ) );
});

module.exports = router;
